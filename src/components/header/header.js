const Header = ({ callAPI }) => {
  return (
    <>
      <h1>Nama Brand</h1>
      <button onClick={callAPI} data-testid="btn-submit">
        Submit
      </button>
    </>
  );
};

export default Header;
